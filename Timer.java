/**
 * Calc timer digited
 * @auhor Walesson Silva
 */
import java.util.Scanner;

public class Timer
{
    static void calcStaticLetter()
    {
        Scanner input = new Scanner(System.in);
        try {
            int number = 0;
            do {
                System.out.println("Informe o número de palavras:");
                number = input.nextInt();
            }while(number <= 0);
            input.nextLine();

            String[] letters = new String[number];
            double[] times = new double[number];

            for(int i = 0; i < number; i++) {
                System.out.println("Informe a palavra "+(i+1)+":");
                letters[i] = input.nextLine();
                times[i] = calcTimerPerLetter(letters[i]); // 1 centésimo de segundo para cada letra digitada
            }

            System.out.println("RESULTADO:");
            for(int i = 0; i < number; i++) {
                System.out.format(letters[i]+" - %.2f segundos\n", times[i]);
            }

        } catch(Exception err) {
            System.out.println("Error: "+err.getMessage());
        }
    }

    static void calcDynamicLetter()
    {
        Scanner input = new Scanner(System.in);
        try {
            int number = 0;
            do {
                System.out.println("Informe o número de palavras:");
                number = input.nextInt();
            }while(number <= 0);
            input.nextLine();

            String[] letters = new String[number];
            double[] times = new double[number];

            for(int i = 0; i < number; i++) {
                double initalTime = System.currentTimeMillis();
                System.out.println("Informe a palavra "+(i+1)+":");
                letters[i] = input.nextLine();
                double endTime = System.currentTimeMillis();
                times[i] = calcTimerSeconds(initalTime, endTime);
            }

            System.out.println("RESULTADO:");
            for(int i = 0; i < number; i++) {
                System.out.format(letters[i]+" - %.2f segundos\n", times[i]);
            }

        } catch(Exception err) {
            System.out.println("Error: "+err.getMessage());
        }
    }

    static double calcTimerSeconds(double initalTime, double endTime)
    {
        return (endTime - initalTime) / 1000;
    }

    static double calcTimerPerLetter(String letters)
    {
        return (double)(letters.length() * 10) / 1000;
    }

    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("################## LETTER COUNT ##################");
        int mode = 0;

        do {
            System.out.println("Escolha um modelo do programa para comecar:");
            System.out.println("1 - Modo estático");
            System.out.println("2 - Modo dinâmico");
            mode = input.nextInt();
            System.out.println(mode);
        } while(mode != 1 && mode != 2);
        
        switch(mode) {
            case 1:
                calcStaticLetter();
                break;
            case 2: 
                calcDynamicLetter();
                break;
            default: 
                System.out.println("Opção inválida!");
        }
    }
}